#RESTful API Node.js Express 4
Este es un RESTful API que usa Node.js &amp; Express 4

##Requirements
* Node and npm

##Installation
* Instalar dependencias: npm install  
* Iniciar el servidor: node server.js



#Use with your existing database
$ npm install -g sequelize-auto

##To generate database, execute this command. Change the arguments to your corresponding values.
$ sequelize-auto -o "./output-folder" -d database_name -h localhost -u username -p port -x my_password -e postgres

sequelize-auto will then generate all the models for you in the output-folder. Later, you can use the import function provided by Sequelize (Sequelize.import) to populate your models. However, as I said before, sequelize-auto only reduces the work that you have to do, not a complete solution so you still have to edit the generated model files.