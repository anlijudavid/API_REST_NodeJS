-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-12-2015 a las 01:25:54
-- Versión del servidor: 5.6.27-0ubuntu1
-- Versión de PHP: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `app_maraton`
--
CREATE DATABASE IF NOT EXISTS `app_maraton` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `app_maraton`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencia`
--

DROP TABLE IF EXISTS `competencia`;
CREATE TABLE IF NOT EXISTS `competencia` (
  `idCompetencia` int(11) NOT NULL,
  `NombreCompetencia` varchar(50) NOT NULL,
  `NroPersonas` int(11) NOT NULL,
  `LimiteInscripcion` date NOT NULL,
  `Fecha` date NOT NULL,
  `HoraInicio` time NOT NULL,
  `HoraFin` time NOT NULL,
  `EnCurso` enum('T','F') NOT NULL,
  `Estado` enum('T','F') NOT NULL,
  `FK_idTCompetencia` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `competencia`
--

INSERT INTO `competencia` (`idCompetencia`, `NombreCompetencia`, `NroPersonas`, `LimiteInscripcion`, `Fecha`, `HoraInicio`, `HoraFin`, `EnCurso`, `Estado`, `FK_idTCompetencia`) VALUES
(1, '8va competencia', 3, '2015-11-30', '2015-11-24', '03:00:00', '16:00:00', 'T', 'T', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

DROP TABLE IF EXISTS `cuenta`;
CREATE TABLE IF NOT EXISTS `cuenta` (
  `idCuenta` int(11) NOT NULL,
  `NUsuario` varchar(45) NOT NULL,
  `Contrasenia` text NOT NULL,
  `FK_idRol` int(11) NOT NULL,
  `FK_idPersona` bigint(20) NOT NULL,
  `FechaRegistro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Estado` enum('T','F') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`idCuenta`, `NUsuario`, `Contrasenia`, `FK_idRol`, `FK_idPersona`, `FechaRegistro`, `Estado`) VALUES
(10, 'anlijudavid@gmail.com', '987', 2, 23, '2015-12-01 23:37:52', 'T');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

DROP TABLE IF EXISTS `cursos`;
CREATE TABLE IF NOT EXISTS `cursos` (
  `idCurso` bigint(20) NOT NULL,
  `NombreCurso` text NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `Cupos` int(11) NOT NULL,
  `FechaInicio` date NOT NULL,
  `FechaFin` date NOT NULL,
  `Estado` enum('T','F') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`idCurso`, `NombreCurso`, `FechaCreacion`, `Cupos`, `FechaInicio`, `FechaFin`, `Estado`) VALUES
(1, 'Ingles', '2015-11-01 00:00:00', 32, '2015-11-10', '2016-05-28', 'T');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejercicios`
--

DROP TABLE IF EXISTS `ejercicios`;
CREATE TABLE IF NOT EXISTS `ejercicios` (
  `idEjercicio` int(11) NOT NULL,
  `NombreEjercicio` varchar(45) NOT NULL,
  `Enunciado` longblob NOT NULL,
  `TipoArchivo` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ejercicios`
--

INSERT INTO `ejercicios` (`idEjercicio`, `NombreEjercicio`, `Enunciado`, `TipoArchivo`) VALUES
(1, 'HelloString', '', 'java');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envios`
--

DROP TABLE IF EXISTS `envios`;
CREATE TABLE IF NOT EXISTS `envios` (
  `idEnvio` int(11) NOT NULL,
  `FK_idEjercicio` int(11) NOT NULL,
  `FK_idGrupo` int(11) NOT NULL,
  `NombreArchivo` text NOT NULL,
  `Fuente` longblob NOT NULL,
  `TipoArchivo` varchar(50) NOT NULL,
  `Calificado` enum('T','F') NOT NULL,
  `Aceptado` enum('T','F') NOT NULL,
  `HoraEnvio` time NOT NULL,
  `FechaEnvio` date NOT NULL,
  `FK_idRespuesta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes_cursos`
--

DROP TABLE IF EXISTS `estudiantes_cursos`;
CREATE TABLE IF NOT EXISTS `estudiantes_cursos` (
  `idECurso` int(11) NOT NULL,
  `FK_idPersona` int(11) NOT NULL,
  `FK_idCurso` bigint(20) NOT NULL,
  `FechaInscripcion` datetime NOT NULL,
  `EstadoInscripcion` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

DROP TABLE IF EXISTS `eventos`;
CREATE TABLE IF NOT EXISTS `eventos` (
  `idEvento` int(11) NOT NULL,
  `NombreEvento` varchar(200) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Comentario` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`idEvento`, `NombreEvento`, `Fecha`, `Comentario`) VALUES
(1, 'Encuentro con RPC ', '2015-12-05 00:00:00', 'Se realizara en encuentro en las instalaciones de la Universidad de la Amazonia'),
(2, 'Reunión general ', '2015-12-04 00:00:00', 'Reunión para asignar tareas grupales. ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

DROP TABLE IF EXISTS `grupos`;
CREATE TABLE IF NOT EXISTS `grupos` (
  `idGrupo` int(11) NOT NULL,
  `NombreGrupo` varchar(50) NOT NULL,
  `UserGrupo` varchar(50) NOT NULL,
  `Contrasenia` varchar(50) NOT NULL,
  `EstadoGrupo` enum('T','F') NOT NULL,
  `FK_idRol` int(11) NOT NULL,
  `FechaInscGrupo` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialejercicios`
--

DROP TABLE IF EXISTS `historialejercicios`;
CREATE TABLE IF NOT EXISTS `historialejercicios` (
  `idHistorial` int(11) NOT NULL,
  `Numero` int(11) NOT NULL,
  `FK_idNivel` int(11) NOT NULL,
  `FK_idCompetencia` int(11) NOT NULL,
  `FK_idEjercicio` int(11) NOT NULL,
  `Estado` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios_cursos`
--

DROP TABLE IF EXISTS `horarios_cursos`;
CREATE TABLE IF NOT EXISTS `horarios_cursos` (
  `idHorario` int(11) NOT NULL,
  `Dia` varchar(50) NOT NULL,
  `HoraInicio` time NOT NULL,
  `HoraFin` time NOT NULL,
  `Estado` enum('T','F') NOT NULL,
  `FK_idCurso` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrantesgrupo`
--

DROP TABLE IF EXISTS `integrantesgrupo`;
CREATE TABLE IF NOT EXISTS `integrantesgrupo` (
  `FK_idPersona` int(11) NOT NULL,
  `FK_idGrupo` int(11) NOT NULL,
  `FK_idCompetencia` int(11) DEFAULT NULL,
  `Estado` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel`
--

DROP TABLE IF EXISTS `nivel`;
CREATE TABLE IF NOT EXISTS `nivel` (
  `idNivel` int(11) NOT NULL,
  `Descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participacion_grupos`
--

DROP TABLE IF EXISTS `participacion_grupos`;
CREATE TABLE IF NOT EXISTS `participacion_grupos` (
  `idParticipacion` bigint(20) NOT NULL,
  `FK_idGrupo` int(11) NOT NULL,
  `FK_idCompetencia` int(11) NOT NULL,
  `FK_idNivel` int(11) NOT NULL,
  `FechaInscrPart` datetime NOT NULL,
  `NEjercicios` int(11) NOT NULL DEFAULT '0',
  `TiempoTotal` time NOT NULL DEFAULT '00:00:00',
  `EstadoPart` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

DROP TABLE IF EXISTS `persona`;
CREATE TABLE IF NOT EXISTS `persona` (
  `idPersona` bigint(20) NOT NULL,
  `TDocumento` int(11) NOT NULL,
  `NDocumento` bigint(20) NOT NULL,
  `Nombres` varchar(45) NOT NULL,
  `Apellidos` varchar(45) NOT NULL,
  `Telefono` bigint(20) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `FK_idPrograma` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idPersona`, `TDocumento`, `NDocumento`, `Nombres`, `Apellidos`, `Telefono`, `Correo`, `FK_idPrograma`) VALUES
(23, 1, 1080364042, 'Julian David', 'Mora Ramos', 3144774531, 'anlijudavid@gmail.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programa`
--

DROP TABLE IF EXISTS `programa`;
CREATE TABLE IF NOT EXISTS `programa` (
  `idPrograma` int(11) NOT NULL,
  `NombrePrograma` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `programa`
--

INSERT INTO `programa` (`idPrograma`, `NombrePrograma`) VALUES
(1, 'Ing. Sistemas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuesta`
--

DROP TABLE IF EXISTS `respuesta`;
CREATE TABLE IF NOT EXISTS `respuesta` (
  `idRespuesta` int(11) NOT NULL,
  `NombreRespuesta` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados`
--

DROP TABLE IF EXISTS `resultados`;
CREATE TABLE IF NOT EXISTS `resultados` (
  `idResultado` int(11) NOT NULL,
  `Puesto` int(11) NOT NULL,
  `Resuelto` enum('T','F') NOT NULL,
  `Penalizaciones` int(11) NOT NULL,
  `Tiempo` int(11) NOT NULL,
  `FK_idGrupo` int(11) NOT NULL,
  `FK_idEjercicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `idRol` int(11) NOT NULL,
  `NombreRol` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idRol`, `NombreRol`) VALUES
(2, 'Estudiante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocompetencia`
--

DROP TABLE IF EXISTS `tipocompetencia`;
CREATE TABLE IF NOT EXISTS `tipocompetencia` (
  `idTCompetencia` int(11) NOT NULL,
  `NombreTCompetencia` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipocompetencia`
--

INSERT INTO `tipocompetencia` (`idTCompetencia`, `NombreTCompetencia`) VALUES
(1, 'Local'),
(2, 'Regional'),
(3, 'Nacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
CREATE TABLE IF NOT EXISTS `tipodocumento` (
  `idTDocumento` int(11) NOT NULL,
  `NTDocumento` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipodocumento`
--

INSERT INTO `tipodocumento` (`idTDocumento`, `NTDocumento`) VALUES
(1, 'CC'),
(2, 'TI');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `competencia`
--
ALTER TABLE `competencia`
  ADD PRIMARY KEY (`idCompetencia`),
  ADD UNIQUE KEY `NombreCompetencia` (`NombreCompetencia`),
  ADD KEY `FK_competencia_tipocompetencia` (`FK_idTCompetencia`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`idCuenta`),
  ADD UNIQUE KEY `NUsuario` (`NUsuario`),
  ADD KEY `fk_Cuenta_Rol1_idx` (`FK_idRol`),
  ADD KEY `fk_Cuenta_Persona1_idx` (`FK_idPersona`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indices de la tabla `ejercicios`
--
ALTER TABLE `ejercicios`
  ADD PRIMARY KEY (`idEjercicio`);

--
-- Indices de la tabla `envios`
--
ALTER TABLE `envios`
  ADD PRIMARY KEY (`idEnvio`),
  ADD KEY `fk_Envios_Grupos1_idx` (`FK_idGrupo`),
  ADD KEY `fk_Envios_Ejercicios1_idx` (`FK_idEjercicio`),
  ADD KEY `fk_Envios_Respuesta1_idx` (`FK_idRespuesta`);

--
-- Indices de la tabla `estudiantes_cursos`
--
ALTER TABLE `estudiantes_cursos`
  ADD PRIMARY KEY (`idECurso`),
  ADD UNIQUE KEY `FK_idPersona` (`FK_idPersona`,`FK_idCurso`),
  ADD KEY `FK_estudiantes_cursos_cursos` (`FK_idCurso`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`idEvento`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`idGrupo`),
  ADD UNIQUE KEY `FK_idGrupo` (`NombreGrupo`),
  ADD UNIQUE KEY `UserGrupo` (`UserGrupo`),
  ADD KEY `FK_grupos_rol` (`FK_idRol`);

--
-- Indices de la tabla `historialejercicios`
--
ALTER TABLE `historialejercicios`
  ADD PRIMARY KEY (`idHistorial`),
  ADD KEY `FK_historialejercicios_ejercicios` (`FK_idEjercicio`),
  ADD KEY `FK_historialejercicios_competencia` (`FK_idCompetencia`),
  ADD KEY `FK_historialejercicios_nivel` (`FK_idNivel`);

--
-- Indices de la tabla `horarios_cursos`
--
ALTER TABLE `horarios_cursos`
  ADD PRIMARY KEY (`idHorario`),
  ADD KEY `FK_horarios_cursos_cursos` (`FK_idCurso`);

--
-- Indices de la tabla `integrantesgrupo`
--
ALTER TABLE `integrantesgrupo`
  ADD UNIQUE KEY `FK_idPersona` (`FK_idPersona`,`FK_idGrupo`,`FK_idCompetencia`) USING BTREE,
  ADD KEY `fk_Persona_has_Grupos_Grupos1_idx` (`FK_idGrupo`),
  ADD KEY `fk_Persona_has_Grupos_Persona1_idx` (`FK_idPersona`),
  ADD KEY `FK_idCompetencia` (`FK_idCompetencia`,`FK_idPersona`);

--
-- Indices de la tabla `nivel`
--
ALTER TABLE `nivel`
  ADD PRIMARY KEY (`idNivel`);

--
-- Indices de la tabla `participacion_grupos`
--
ALTER TABLE `participacion_grupos`
  ADD PRIMARY KEY (`idParticipacion`),
  ADD KEY `FK_participacion_grupos_grupos` (`FK_idGrupo`),
  ADD KEY `FK_participacion_grupos_competencia` (`FK_idCompetencia`),
  ADD KEY `FK_participacion_grupos_nivel` (`FK_idNivel`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD UNIQUE KEY `Nombres` (`Nombres`,`Apellidos`),
  ADD UNIQUE KEY `NDocumento` (`NDocumento`),
  ADD UNIQUE KEY `Correo` (`Correo`),
  ADD KEY `fk_Persona_Programa1_idx` (`FK_idPrograma`),
  ADD KEY `FK_persona_tipodocumento` (`TDocumento`);

--
-- Indices de la tabla `programa`
--
ALTER TABLE `programa`
  ADD PRIMARY KEY (`idPrograma`);

--
-- Indices de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  ADD PRIMARY KEY (`idRespuesta`);

--
-- Indices de la tabla `resultados`
--
ALTER TABLE `resultados`
  ADD PRIMARY KEY (`idResultado`),
  ADD KEY `fk_Resultados_Grupos1_idx` (`FK_idGrupo`),
  ADD KEY `fk_Resultados_Ejercicios1_idx` (`FK_idEjercicio`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `tipocompetencia`
--
ALTER TABLE `tipocompetencia`
  ADD PRIMARY KEY (`idTCompetencia`);

--
-- Indices de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  ADD PRIMARY KEY (`idTDocumento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `competencia`
--
ALTER TABLE `competencia`
  MODIFY `idCompetencia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `idCuenta` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `idCurso` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `ejercicios`
--
ALTER TABLE `ejercicios`
  MODIFY `idEjercicio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `envios`
--
ALTER TABLE `envios`
  MODIFY `idEnvio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estudiantes_cursos`
--
ALTER TABLE `estudiantes_cursos`
  MODIFY `idECurso` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `idEvento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `idGrupo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `historialejercicios`
--
ALTER TABLE `historialejercicios`
  MODIFY `idHistorial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `horarios_cursos`
--
ALTER TABLE `horarios_cursos`
  MODIFY `idHorario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `nivel`
--
ALTER TABLE `nivel`
  MODIFY `idNivel` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `participacion_grupos`
--
ALTER TABLE `participacion_grupos`
  MODIFY `idParticipacion` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `programa`
--
ALTER TABLE `programa`
  MODIFY `idPrograma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  MODIFY `idRespuesta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `resultados`
--
ALTER TABLE `resultados`
  MODIFY `idResultado` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipocompetencia`
--
ALTER TABLE `tipocompetencia`
  MODIFY `idTCompetencia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  MODIFY `idTDocumento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `competencia`
--
ALTER TABLE `competencia`
  ADD CONSTRAINT `FK_competencia_tipocompetencia` FOREIGN KEY (`FK_idTCompetencia`) REFERENCES `tipocompetencia` (`idTCompetencia`);

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `fk_Cuenta_Persona1` FOREIGN KEY (`FK_idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Cuenta_Rol1` FOREIGN KEY (`FK_idRol`) REFERENCES `rol` (`idRol`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `envios`
--
ALTER TABLE `envios`
  ADD CONSTRAINT `fk_Envios_Ejercicios1` FOREIGN KEY (`FK_idEjercicio`) REFERENCES `ejercicios` (`idEjercicio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Envios_Grupos1` FOREIGN KEY (`FK_idGrupo`) REFERENCES `grupos` (`idGrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Envios_Respuesta1` FOREIGN KEY (`FK_idRespuesta`) REFERENCES `respuesta` (`idRespuesta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `estudiantes_cursos`
--
ALTER TABLE `estudiantes_cursos`
  ADD CONSTRAINT `FK_estudiantes_cursos_cursos` FOREIGN KEY (`FK_idCurso`) REFERENCES `cursos` (`idCurso`),
  ADD CONSTRAINT `FK_estudiantes_cursos_persona` FOREIGN KEY (`FK_idPersona`) REFERENCES `cuenta` (`idCuenta`);

--
-- Filtros para la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD CONSTRAINT `FK_grupos_rol` FOREIGN KEY (`FK_idRol`) REFERENCES `rol` (`idRol`);

--
-- Filtros para la tabla `historialejercicios`
--
ALTER TABLE `historialejercicios`
  ADD CONSTRAINT `FK_historialejercicios_competencia` FOREIGN KEY (`FK_idCompetencia`) REFERENCES `competencia` (`idCompetencia`),
  ADD CONSTRAINT `FK_historialejercicios_ejercicios` FOREIGN KEY (`FK_idEjercicio`) REFERENCES `ejercicios` (`idEjercicio`),
  ADD CONSTRAINT `FK_historialejercicios_nivel` FOREIGN KEY (`FK_idNivel`) REFERENCES `nivel` (`idNivel`);

--
-- Filtros para la tabla `horarios_cursos`
--
ALTER TABLE `horarios_cursos`
  ADD CONSTRAINT `FK_horarios_cursos_cursos` FOREIGN KEY (`FK_idCurso`) REFERENCES `cursos` (`idCurso`);

--
-- Filtros para la tabla `integrantesgrupo`
--
ALTER TABLE `integrantesgrupo`
  ADD CONSTRAINT `FK_integrantesgrupo_competencia` FOREIGN KEY (`FK_idCompetencia`) REFERENCES `competencia` (`idCompetencia`),
  ADD CONSTRAINT `FK_integrantesgrupo_cuenta` FOREIGN KEY (`FK_idPersona`) REFERENCES `cuenta` (`idCuenta`),
  ADD CONSTRAINT `fk_Persona_has_Grupos_Grupos1` FOREIGN KEY (`FK_idGrupo`) REFERENCES `grupos` (`idGrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `participacion_grupos`
--
ALTER TABLE `participacion_grupos`
  ADD CONSTRAINT `FK_participacion_grupos_competencia` FOREIGN KEY (`FK_idCompetencia`) REFERENCES `competencia` (`idCompetencia`),
  ADD CONSTRAINT `FK_participacion_grupos_grupos` FOREIGN KEY (`FK_idGrupo`) REFERENCES `grupos` (`idGrupo`),
  ADD CONSTRAINT `FK_participacion_grupos_nivel` FOREIGN KEY (`FK_idNivel`) REFERENCES `nivel` (`idNivel`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `FK_persona_tipodocumento` FOREIGN KEY (`TDocumento`) REFERENCES `tipodocumento` (`idTDocumento`),
  ADD CONSTRAINT `fk_Persona_Programa1` FOREIGN KEY (`FK_idPrograma`) REFERENCES `programa` (`idPrograma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `resultados`
--
ALTER TABLE `resultados`
  ADD CONSTRAINT `fk_Resultados_Ejercicios1` FOREIGN KEY (`FK_idEjercicio`) REFERENCES `ejercicios` (`idEjercicio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Resultados_Grupos1` FOREIGN KEY (`FK_idGrupo`) REFERENCES `grupos` (`idGrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
