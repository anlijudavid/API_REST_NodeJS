/**
 * FRAMEWORK EXPRESS DE Node.js
 * Realizado por Julian David MR
 */
var database = require('./database.json');
var app = require('express')();
var http = require('http').Server(app);
var mysql = require('mysql');
var bodyParser = require("body-parser");


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Puerto de escucha
var puerto = 3000;

var connection = mysql.createConnection(database.local);

connection.connect(function (err) {
	if (!err) {
		console.log("Database conectada ...\n");
	} else {
		console.log("Error al conectar database ... \n\n");
	}
});

/** 
 * Funcion para acortar la escritura del metodo console.log(...);
 */
function m(ms) {
	console.log(ms);
}

/**
 * Funcion que permite dar permisos de acceso al publico, se podran realizar peticiones 
 * desde cualquier direccion.
 * Si este metodo no se usa, entonces solamente se podrian realizar solicitudes locales. 
 */
function permitir_todo(res) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');	
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
	res.setHeader("Content-Type", "text/html");
}

var permiso = 'permiso';
var programa = 'programa';

//Solicitud raiz
app.get('/', function (req, res) {
	permitir_todo(res);
	var data = {
		"Data": "",
		"OS": ""
	};
	data["Data"] = "Bienvenido al semillero de programacion...";
	data["OS"] = "Usted esta usando: " + req.get('user-agent');
	res.json(data);
});

var models = [
	'competencia',
	'cuenta',
	'cursos',
	'ejercicios',
	'envios',
	'estudiantes_cursos',
	'grupos',
	'historialejercicios',
	'horarios_cursos',
	'integrantesgrupo',
	'nivel',
	'participacion_grupos',
	'persona',
	'programa',
	'respuesta',
	'resultados',
	'rol',
	'tipocompetencia',
	'tipodocumento',
	'eventos'
];

app.get('/get/:tabla', function (req, res) {
	var tabla = req.params.tabla;
	console.log("Metodo de consulta general. Consultando " + tabla);

	permitir_todo(res);
	models.forEach(function (m) {
		if (m == tabla) {
			connection.query("SELECT * from " + tabla + ";", function (err, rows, fields) {
				if (err) {
					res.status(100);
					res.send("Error al consultar las .");
				} else if (rows.length != 0) {
					res.status(200);
					res.json(rows);
				} else {
					res.status(100);
					res.send("No hay cuentas registradas.");
				}
			});
		}
	}, this);
});

//SELECT * FROM cuenta INNER JOIN persona as p ON p.idPersona = cuenta.FK_idPersona 
app.get('/cuenta_persona/get', function (req, res) {
	console.log("Metodo de consulta de cuenta combinada con persona");
	permitir_todo(res);
	connection.query("SELECT * FROM cuenta INNER JOIN persona ON persona.idPersona = cuenta.FK_idPersona;", function (err, rows, fields) {
		if (err) {
			res.status(100);
			res.send("Error al consultar las .");
		} else if (rows.length != 0) {
			res.status(200);
			res.json(rows);
		} else {
			res.status(100);
			res.send("No hay cuentas registradas.");
		}
	});
});


/**Iniciar sesion:
 * Consultar un usuario por email y contrasenia
 * Retorna un json si con los datos de la persona
 **/
app.get('/usuarios/auth/:email/:pass', function (req, res) {
	permitir_todo(res);
	var email = req.params.email;
	var pass = req.params.pass;

	var data = {
		"error": 1,
		"usuarios": "Valor por defecto de Json no cambiado."
	};

	console.log("Iniciando sesión. Credenciales: " + email + " - " + pass);
	if (!!email && !!pass) {
		var sql = "SELECT * FROM cuenta	INNER JOIN persona ON persona.idPersona = cuenta.FK_idPersona && cuenta.NUsuario='" + email + "' && cuenta.Contrasenia='" + pass + "';";
		connection.query(sql, function (err, rows, fields) {
			if (rows.length != 0) {
				console.log("Inicio sesion: " + rows);
				res.json(rows);
			} else {
				console.log("No se pudo iniciar sesion: " + rows);
				res.send("error");
			}
		});
	} else {
		data["usuarios"] = "Por favor verifica los datos";
		res.json(data);
	}
});

app.post('/persona/actualizar/:idPersona/:fk_idTipoDocumento/:NDocumento/:Nombres/:Apellidos/:Telefono/:Correo/:fk_idPrograma', function (req, res) {
	console.log("Entro al metodo de actualizar persona");
	permitir_todo(res);
	//variables de persona
	var idPersona = req.params.idPersona;
	var fk_idTipoDocumento = req.params.fk_idTipoDocumento;
	var NDocumento = req.params.NDocumento;
	var Nombres = req.params.Nombres;
	var Apellidos = req.params.Apellidos;
	var Telefono = req.params.Telefono;
	var Correo = req.params.Correo;
	var fk_idPrograma = req.params.fk_idPrograma;

	if (!!fk_idTipoDocumento && !!NDocumento && !!Nombres && !!Nombres && !!Apellidos && !!Telefono
		&& !!Correo && !!fk_idPrograma) {

		//Personas
		var sql = "UPDATE persona SET TDocumento=?,NDocumento=?,Nombres=?,Apellidos=?,Telefono=?,Correo=?,FK_idPrograma=? WHERE idPersona=?";
		connection.query(sql, [fk_idTipoDocumento, NDocumento, Nombres, Apellidos, Telefono, Correo, fk_idPrograma, idPersona], function (err, rows) {
			if (err) {
				res.status(100);
				res.send("Error Adding data, rollback aplicado." + err);
			} else {
				console.log("Se actualizaron los datos de persona.");
			}
		});
	} else {
		res.status(100);
		res.send("Por favor, debes ingresar todos los datos requeridos correctamente.");
	}
});

//Ingresar usuario
app.post('/cuenta/add/:NUsuario/:Contrasenia/:fk_idTipoDocumento/:NDocumento/:Nombres/:Apellidos/:Telefono/:Correo/:fk_idPrograma', function (req, res) {
	console.log("Entro al metodo de insertar persona");
	permitir_todo(res);
	//variables de cuenta
	var NUsuario = req.params.NUsuario;
	var Contrasenia = req.params.Contrasenia;
	var fk_idTipoDocumento = req.params.fk_idTipoDocumento;
	var NDocumento = req.params.NDocumento;
	var Nombres = req.params.Nombres;
	var Apellidos = req.params.Apellidos;
	var Telefono = req.params.Telefono;
	var Correo = req.params.Correo;
	var fk_idPrograma = req.params.fk_idPrograma;
	var fk_idRol = 2; //Default
	var Estado = 'T';

	//Persona:  idPersona TDocumento NDocumento Nombres Apellidos Telefono Correo FK_idPrograma
	//Cuenta: 	idCuenta NUsuario Contrasenia FK_idRol FK_idPersona FechaRegistro Estado 
	if (!!NUsuario && !!Contrasenia && !!fk_idTipoDocumento
		&& !!NDocumento && !!Nombres && !!Apellidos
		&& !!Telefono && !!Correo && !!fk_idPrograma && !!fk_idRol) {
		var sql = "INSERT INTO persona(TDocumento, NDocumento, Nombres, Apellidos, Telefono, Correo, FK_idPrograma) VALUES(?,?,?,?,?,?,?);";
		connection.query(sql, [fk_idTipoDocumento, NDocumento, Nombres, Apellidos, Telefono, Correo, fk_idPrograma], function (err, rows) {
			if (err) {
				console.log("ERROR al insertar la persona de la cuenta.");
				res.status(500);
				res.send("ERROR al insertar la persona de la cuenta.");
				return;
			} else {
				console.log("Se insertaron los datos de persona.");
			}
										
			//Si llega hasta acá es porque ingresó la persona. Procedemos a consultar el ID de esa persona.
			var sql = "SELECT * from persona WHERE NDocumento='" + NDocumento + "' && Correo='" + Correo + "' && Nombres='" + Nombres + "';";
			console.log("SQL: " + sql);
			connection.query(sql, function (err, rows, fields) {
				if (err) {
					console.log("Error al realizar la consulta, " + err);
					res.send("Error al realizar la consulta, " + err);
					return;
				} else if (rows.length != 0) {
					console.log("Filas consultadas: " + rows);
					var Fk_idPersona = rows[0].idPersona;
					console.log(Fk_idPersona + " --- " + rows)
					//Insertando dato en la cuenta	
					var sql = "INSERT INTO cuenta(NUsuario, Contrasenia, FK_idRol, FK_idPersona, Estado) VALUES(?,?,?,?,?);";
					connection.query(sql, [NUsuario, Contrasenia, fk_idRol, Fk_idPersona, Estado], function (err, rows) {
						if (err) {
							console.log("ERROR al insertar cuenta." + err);
							res.status(500);
							res.send("ERROR al insertar cuenta.");
							return;
						} else {
							console.log("Se insertaron los datos de persona.");
							res.send("Se insertaron los datos de persona.");
						}
					});
				} else {
					console.log("Error al iniciar sesion: " + rows);
					res.send("Error al iniciar sesion: " + rows);
				}
			});
		});
	} else {
		console.log("Por favor, debes ingresar todos los datos requeridos correctamente.");
		res.json("Por favor, debes ingresar todos los datos requeridos correctamente.");
	}
});

app.get('/envios/:idUsuario', function (req, res) {
	console.log("Consultando todos los envios.");
	permitir_todo(res);
	connection.query("SELECT * from envio", function (err, rows, fields) {
		if (err) {
			res.status(100);
			res.send("Error al consultar envios.");
		} else if (rows.length != 0) {
			res.status(200);
			res.json(rows);
		} else {
			res.status(100);
			res.send("No hay envios registrados.");
		}
	});
});

//Ingresar grupo
app.post('/grupo/add/:NombreGrupo/:UserGrupo/:Contrasenia/:FK_idPersona1/:FK_idPersona2/:FK_idPersona3/:FK_idCompetencia', function (req, res) {
	console.log("Entro al metodo de insertar grupo");
	permitir_todo(res);

	// Grupo: idGrupo NombreGrupo UserGrupo Contrasenia	EstadoGrupo FK_idRol FechaInscGrupo
	// Integrante grupo:  FK_idPersona, FK_idGrupo, FK_idCompetencia, FK_idParticipacion, Estado
	var NombreGrupo = req.params.NombreGrupo;
	var UserGrupo = req.params.UserGrupo;
	var Contrasenia = req.params.Contrasenia;
	var FK_idPersona1 = req.params.FK_idPersona1;
	var FK_idPersona2 = req.params.FK_idPersona2;
	var FK_idPersona3 = req.params.FK_idPersona3;
	var FK_idCompetencia = req.params.FK_idCompetencia;

	var fk_idRol = 2; //Default
	var Estado = 'T';

	var List_fk_idPersonas = [FK_idPersona1, FK_idPersona2, FK_idPersona3];

	if (!!NombreGrupo && !!Contrasenia && !!UserGrupo
		&& !!FK_idPersona1 && !!FK_idPersona2 && !!FK_idPersona3
		&& !!FK_idCompetencia) {

		res.setHeader("Content-Type", "text/html");

		var sql = "INSERT INTO grupos(NombreGrupo,UserGrupo,Contrasenia,EstadoGrupo,FK_idRol,FechaInscGrupo) VALUES(?,?,?,?,?,now());";
		connection.query(sql, [NombreGrupo, UserGrupo, Contrasenia, Estado, fk_idRol], function (err, rows) {
			if (err) {
				console.log("ERROR al insertar el grupo.");
				res.status(500);
				res.send("ERROR al insertar el grupo.");
				return;
			} else {
				console.log("Se insertaron los datos del grupo.");
			}

			var sql = "SELECT * from grupos WHERE NombreGrupo='" + NombreGrupo + "' && UserGrupo='" + UserGrupo + "' && Contrasenia='" + Contrasenia + "';";
			console.log("SQL: " + sql);
			connection.query(sql, function (err, rows, fields) {
				if (err) {
					console.log("Error al realizar la consulta, " + err);
					res.send("Error al realizar la consulta, " + err);
					return;
				} else if (rows.length != 0) {
					console.log("Filas consultadas: " + rows);
					var Fk_idGrupo = rows[0].idGrupo;
					console.log(Fk_idGrupo + " --- " + rows[0])
					try {
						var sql = "INSERT INTO integrantesgrupo(FK_idPersona,FK_idGrupo,FK_idCompetencia,Estado) VALUES";
						List_fk_idPersonas.forEach(function (idpersona) {
							sql += "(" + idpersona + "," + Fk_idGrupo + "," + FK_idCompetencia + ",'" + Estado + "'),";
						}, this);
						sql = sql.slice(0, sql.length - 1) + ";";
						console.log("El insert: " + sql);
						connection.query(sql, function (err, rows) {
							if (err) {
								console.log("ERROR al insertar el integrante." + err);
								res.send("ERROR al insertar el integrante." + err);
							} else {
								console.log("Se ingresaron los integrantes");
								res.send("Se ingresaron los integrantes");
							}
						});
					} catch (error) {
						console.log("Error al insertar los integrantes del grupo: " + err);
						res.send("Error al insertar los integrantes del grupo: " + err);
					}
				} else {
					console.log("Error al consultar datos: " + rows);
					res.send("Error al consultar datos: " + rows);
				}
			});
		});
	} else {
		console.log("Por favor, debes ingresar todos los datos requeridos correctamente.");
		res.json("Por favor, debes ingresar todos los datos requeridos correctamente.");
	}
});
 
//Ingresar integrantes de grupo
var integrantes_grupo = "integrantesgrupo";

app.post('/grupo/integrantes/insertar/:FK_idGrupo/:FK_idCompetencia/:FK_idParticipacion/:FK_idPersona?', function (req, res) {
	console.log("Entro al metodo de insertar integrantes de grupo.");
	permitir_todo(res);

	var FK_idGrupo = req.params.FK_idGrupo;
	var FK_idCompetencia = req.params.FK_idCompetencia;
	var FK_idParticipacion = req.params.FK_idParticipacion;
	var FK_idPersona = req.params.FK_idPersona;
	var Estado = 'T';

	m(req.params);
	if (!!FK_idGrupo && !!FK_idCompetencia && !!Estado && !!FK_idParticipacion && !!FK_idPersona) {

		var IDsPersonas = FK_idPersona;
		m("- " + IDsPersonas);
		var IDs = IDsPersonas.split(';');

		var sql = "";

		for (var i = 0; i < IDs.length; i++) {
			var idPersona = IDs[i];
			m("* " + idPersona);
			sql += "INSERT INTO " + integrantes_grupo + "(FK_idPersona,FK_idGrupo,FK_idCompetencia,FK_idParticipacion, Estado) VALUES(" + idPersona + "," + FK_idGrupo + "," + FK_idCompetencia + "," + FK_idParticipacion + "," + Estado + ");";
		}

		m("SQL concat: " + sql.toString());
		res.status(200);
		res.send(" ++ " + sql);

		connection.query(sql, function (err, rows, fields) {
			if (err) {
				res.status(100);
				res.send("Error al realizar la consulta, " + err);
			} else if (rows.length != 0) {
				console.log("Filas consultadas: " + rows);
				res.json(rows);
			} else {
				res.status(100);
				res.send("No se encontraron datos de " + grupo);
			}
		});
	} else {
		res.status(404);
		res.send("Por favor, debes ingresar todos los datos requeridos correctamente.");
	}
});


app.all('*', function (req, res) {
	permitir_todo(res);
	res.write('Solicitud erronea: *\n');
	res.write('ERR, no regex match\n');
	res.write(req.body);
	res.end();
});


/**
		 *.							  Activando escucha
		 *  						Corriendo el servidor
		 */
//var ip = '192.168.1.39';		
var ip = '127.0.0.1';
http.listen(puerto, ip, function () {
	console.log('Servidor NodeJS APP corriendo.');
	console.log("Conectado & Escuchando el puerto " + puerto);
	console.log("Ingrese a http://" + ip + ":" + puerto);
});